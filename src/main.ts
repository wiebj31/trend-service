import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { AppModule } from './trend.module';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';

const logger = new Logger('Main');

const microservicesOptions = {
  transport: Transport.REDIS,
  options: {
    url: 'redis://localhost:6379',
    auth_pass:'redispassword'
  },
} as MicroserviceOptions; 

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors()
  app.connectMicroservice<MicroserviceOptions>(microservicesOptions);
  app.listen(3002,() => {
    logger.log('microservice is listening on port 3002 ...');
  });
}
bootstrap();