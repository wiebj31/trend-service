import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository, } from '@nestjs/typeorm';
import { getRepository, Like, Repository } from 'typeorm';
import { Trend } from './Entity/Trend';
import { ClientProxy, Ctx, EventPattern, MessagePattern, Payload, RedisContext} from '@nestjs/microservices';


@Injectable()
export class TrendService {

  constructor(
    @InjectRepository(Trend)
    private trendRepository: Repository<Trend>) 
  {}

  async getByNameLoose(trendname: string) {
    const response = await this.trendRepository
      .createQueryBuilder('trend')
      .where("name like :name", { name: `%${trendname}%` });
    return response.getMany();
  }

  async getByName(trendname: string) {
    return this.trendRepository.findOne({'name':trendname});
  }

  async getPopular(){
    const take = 10;
    const skip = 0;
    const [result, total] = await this.trendRepository.findAndCount(
      {
          take: take,
          skip: skip,
          order: {usage: 'DESC'},
      });
    return {data: result, count: total};
  }

  async createTrend(trend: Trend) {
    return this.trendRepository.save(trend);
  }

  async deleteTrend(trendId: string) {
    return this.trendRepository.delete(trendId);
  }

  async checkForNewTrend(trends: any) {
    trends.forEach(async element => {
      if(!await this.getByName(element)){
        const trend = new Trend();
        trend.name = element;
        trend.created = new Date()
        trend.usage = 1;
        await this.createTrend(trend)
      }
    });
    return trends;
  }

  async updateTrendUsage(trends: string[]){
    trends.forEach(async element=>{
      let trend = await this.getByName(element);
      trend.usage++;
      this.trendRepository.save(trend);
    })
  }

  
}
