import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class Trend {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    name!: string;

    @Column('datetime')
    created!: Date;

    @Column({default: 0})
    usage: number;
}

export class checkForNewTrendDTO{
    trends: string[];
}