import { Body, Controller, Delete, Get, Inject, Param, Post } from '@nestjs/common';
import { TrendService } from './trend.service';
import { checkForNewTrendDTO, Trend } from './Entity/Trend';
import { get } from 'node:http';
import { response } from 'express';
import { ClientProxy, EventPattern, MessagePattern, Payload } from '@nestjs/microservices';
import { ConfigService } from '@nestjs/config';

@Controller()
export class TrendController {
  constructor(private readonly TrendService: TrendService, private configservice: ConfigService) {
  }

  @Get('/trend/:name')
  async getByName(@Param('name') name: string){
    const data = this.TrendService.getByNameLoose(name);
    const response = new Array();
    (await data).forEach(element =>{
      response.push(element.name)
    })
    return response
  }

  @Get('/trend_popular')
  async getPopularTrend(){
    return await this.TrendService.getPopular();
  }

  @Post('/trend')
  async createTrend(@Body() trend: Trend){
    const response = await this.TrendService.getByName(trend.name);
    if(response == undefined){
      return this.TrendService.createTrend(trend);
    }
    return 'Trend already exists';
  }

  @Delete('/trend/:id')
  async deleteTrend(@Param('id') trendId: string){
    console.log(trendId)
    return this.TrendService.deleteTrend(trendId);
  }
  
  @Post('/trend/check')
  async checkForNewTrend(@Body() trend: object){
    return await this.TrendService.checkForNewTrend(trend).then(res =>{
      this.TrendService.updateTrendUsage(res);
      return res
    });
  }

  @MessagePattern('tweet_created')
  updateTrendUsage(@Payload() data: string[]) {
    console.log('hey');
  }
}
