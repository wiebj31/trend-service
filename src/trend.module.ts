import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { TrendController } from './trend.controller';
import { TrendService } from './trend.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Trend } from './Entity/Trend';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({

  imports: [ClientsModule.register([
    {
      name: 'TREND_SERVICE',
      transport: Transport.REDIS,
      options: {
        url: 'redis://localhost:6379',
        auth_pass: 'redispassword'
      }
    },
  ]),
  ConfigModule.forRoot({isGlobal:true}),
  TypeOrmModule.forRoot({
    type: 'mysql',
    host: '178.84.152.77',
    port: 3307,
    username: 'root',
    password: 'wachtwoord',
    database: 'Trend',
    entities: [Trend],
    synchronize: true,
  }),
  
  TypeOrmModule.forFeature([Trend])],
  controllers: [TrendController],
  providers: [TrendService],
})
export class AppModule { }
