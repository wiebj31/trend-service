import { Test, TestingModule } from '@nestjs/testing';
import { TrendController } from './trend.controller';
import { TrendService } from './trend.service';
import { Repository } from 'typeorm';
import { Trend } from './Entity/Trend';

describe('AppController', () => {
  let appController: TrendController;
  let trendService: TrendService;

  beforeEach(async () => {
    trendService = new TrendService(new Repository<Trend>());

    const app: TestingModule = await Test.createTestingModule({
      providers: [TrendService],
    }).overrideProvider(TrendService)
    .useValue(trendService)
    .compile();

  });

  describe('Test', () => {
    it('test test', async () => {
      const test = 'test'
      const test_two = 'test'

      expect(test).toBe(test_two);
    });
  });
});
